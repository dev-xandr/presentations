## PHP syntax. Day 1

---
##### Comment types
```php
<?php 

// one line comment
#  one line comment

/* multiline 
 * comments 
 */
```

---
##### Variable definition
```php
<?php 
# variable declaration
#   |       variable content
#   |         |
$someText = "Hello PHP world!"; # usually line should end with column character (;), but not all of them

// basic types
$number  = 5;       # variable of integer/number type
$decimal = 5.03;    # variable of float/real/decimal type
$string  = "some text"; # variable of string type
$array   = [$number, 12, $string]; # array of items (you can mix types)

```

---
##### Function definition
```php
<?php 
# function definition
#   |   function name
#   |       |    function arguments
#   |       |          |   |
function calculateSum($a, $b) {
#                             |
# function body start --------+

    return $a + $b; # <---- function body

# function body end
# / 
}

# function call
calculateSum(3, 5); // will return 8
```