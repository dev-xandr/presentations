## Action autowiring
---
##### Now
```php
<?php
// Config/map.php

$this->map('my-path/\Z', '\\Somewhere\\Rest\\MyRestHandler');
//... other mappings
```
```php
<?php
// MyRestHandler.php
class MyRestHandler {
    
    public function handleGet(){
    // call controller and log something
    }
    
    public function handlePost(){
    // call controller and log something
    }
    
    //... and so on
}
```
---
##### Someday
```php
<?php
class Controller {
    
    // direct injection
    public function buy(PaymentService $paymentService, stdClass $order){
        $paymentService->processOrder($order);
    }
    // .. other methods with other services
}
```

---
##### Benefits

@ul
- Only required instances are created during request
- Thinner constructor
- No more `buildMeAnyInstanceIWant()` methods
@ulend

---
##### Problems

@ul
- Autowiring is turnded off becuase of Soap classmap (initial and very old problem)
- A lot of legacy methods that should be refactored
- Time and attention is required
@ulend