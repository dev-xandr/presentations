## Action autowiring
---
##### Now
```php
<?php
class Controller {
    
    /** @var PaymentService */
    protected $paymentService;
    
    // constructor injection
    public function __construct(
        PaymentService $paymentService,
        UserService $UserService // and a lot of other services
    ) {
        // ...
    }
    
    public function buy(stdClass $order){
        $this->paymentService->processOrder($order);
    }
    // .. other methods with other services
}
```
---
##### Someday
```php
<?php
class Controller {
    
    // direct injection
    public function buy(PaymentService $paymentService, stdClass $order){
        $paymentService->processOrder($order);
    }
    // .. other methods with other services
}
```

---
##### Benefits

@ul
- Only required instances are created during request
- Thinner constructor
- No more `buildMeAnyInstanceIWant()` methods
@ulend

---
##### Problems

@ul
- Autowiring is turnded off becuase of Soap classmap (initial and very old problem)
- A lot of legacy methods that should be refactored
- Time and attention is required
@ulend