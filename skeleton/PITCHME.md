# Let's Get Started

---

## Add Some Slide Candy

![](assets/img/presentation.png)

---
@title[Customize Slide Layout]

@snap[west span-50]
## Customize the Layout
@snapend

@snap[east span-50]
![](assets/img/presentation.png)
@snapend

@snap[south span-100 text-white]
Snap Layouts let you create custom slide designs directly within your markdown.
@snapend

---

##### Before
```php
<?php
$var = 'hi there';
// comment
```
##### After
```php
<?php
$var = 'hi there';
// comment
```
